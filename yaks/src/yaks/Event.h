/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2014, ReTiS Lab., Scuola Superiore Sant'Anna.
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the ReTiS Lab. nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

/**
 * \file Event.h
 * \brief Object adapter for RTSim kernels
 * \authors:
 * \        Matteo Morelli matteo.morelli@sssup.it
 * \        Fabio Cremona fabio.cremona@sssup.it
 * \        Marco Di Natale marco.dinatale@sssup.it
 * \date February 2014
 */

#ifndef YAKS_EVENT_HDR
#define YAKS_EVENT_HDR
#include <string>
#include "SimTask.h"

namespace yaks
{
    /**
     * \addtogroup yaks_interface
     * @{
     */
    /**
     * Event types
     * FIXME. This solution does not scale for new events
     *        to be defined. Changing the type to a string
     *        and specifying it at registration time
     *       (factory pattern) will fix this issue.
     */
    enum class EventType
    {
        END_INSTRUCTION,
        END_TASK,
        PREEMPTION,
        OTHER
    };
    
    /**
     * \brief The base class for every event
     */
    class Event
    {

    public:

        typedef std::string BASE_KEY_TYPE;

        /**
         * \brief Default constructor
         */
        Event();

        /**
         * \brief The virtual destructor
         */
        virtual ~Event();

        /**
         * \brief Get event name
         */
        virtual std::string getName() const = 0;

        /**
         * \brief Get time of event occurrence
         * TODO. return int?
         */
        virtual long int getTime() const = 0;

        /**
         * Get the event type
         */
        virtual EventType getType() const = 0;

        /**
         * Get a pointer to the task which has generated this event
         */
        virtual SimTask* getGeneratorTask() = 0;
    };
    /** @} */

}

#endif // YAKS_EVENT_HDR
