/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2014, ReTiS Lab., Scuola Superiore Sant'Anna.
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the ReTiS Lab. nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

/**
 * \file XmlDescription.h
 * \brief An \em abstract class that represents an XML application description
 * \author Matteo Morelli matteo.morelli@sssup.it
 * \date December 2013
 */
#ifndef YAKS_XMLDESCRIPTION_HDR
#define YAKS_XMLDESCRIPTION_HDR
#include <string>
#include <map>
#include <tinyxml.h>

namespace yaks
{

    /**
     * \addtogroup yaks_core_utils
     * @{
     */

    /**
     * \brief Class that encodes the parsed information of the XML application
     * description
     *
     * This class parses an XML file containing the application description and
     * saves the information in an associative container (std::map). This class
     * relies on the capabilities of TinyXML for parsing the XML file.
     *
     * An instance of yaks::XmlDescription encapsulates an instance of TiXmlDocument
     * and a std::map. The TiXmlDocument is the TinyXML representation of the XML
     * application description; the std::map stores the elements formed by the
     * combination of a std::string (the key value) and a pointer to a TiXmlElement
     * (the mapped value). The i-th key value is the ValueStr() (i.e., the name) of
     * the i-th FirstChildElement() of the root element in the TiXmlDocument (i.e.,
     * the element named "description"); the mapped value is a pointer to the TinyXML
     * sub-tree for that element container.
     *
     * More specifically, assume the following is the application description:
     *
     * \code
     * anAppDescription.xml
     * ====================
     *
     * <?xml version="0.1.0" ?>
     * <description>
     *
     *     <!-- ... -->
     *
     * </description>
     * \endcode
     *
     * When an instance of yaks::XmlDescription is constructed
     *
     * \code
     * XmlDescription xml_descr("anAppDescription.xml");
     * \endcode
     *
     * the std::map contains the following information:
     *
     * \anchor xml_description_example \image html xml_description_example.png "The internal state of an instance of a yaks::XmlDescription object for the given application description"
     *
     * Note that, parsing the XML sub-trees is delegated to the create() methods of
     * factory classes (and then to the concrete-product constructors), since they
     * know the logics for doing it correctly. Delegation improves the extensibility
     * of design.
     */
    class XmlDescription
    {

        public:

            /**
             * \brief Constructor
             * \param fn The name of the XML file
             * \throws yaks::Exception if the file cannot be found
             */
            XmlDescription(const std::string& fn);

            /**
             * \brief Virtual destructor
             */
            virtual ~XmlDescription() {};

            /**
             * \brief Utility function for extracting info from the internal std::map
             * \param key The key value
             * \return The corresponding mapped value
             */
            TiXmlElement* getValueFromKey(const std::string& key) const;

        protected:

            /** The internal instance of the XML document parsed by TinyXML */
            TiXmlDocument xml_doc;

            /** The internal associative container (std::map) */
            std::map<std::string, TiXmlElement*> description_table;
    };

    /** @} */

}

#endif // YAKS_XMLDESCRIPTION_HDR
