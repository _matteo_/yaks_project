/***************************************************************************
    begin                : Thu Apr 24 15:54:58 CEST 2003
    copyright            : (C) 2003 by Giuseppe Lipari
    email                : lipari@sssup.it
 ***************************************************************************/
/***************************************************************************
 *                                                                         *
 *   This file is free software; you can redistribute it and/or            *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 ***************************************************************************/
/*
 * $Id: factory.hpp,v 1.3 2005/04/28 01:34:47 cesare Exp $
 *
 * $Log: factory.hpp,v $
 * Revision 1.3  2005/04/28 01:34:47  cesare
 * Moved to sstream. Headers install. Code cleaning.
 *
 * Revision 1.2  2004/11/22 00:35:24  cesare
 * *** empty log message ***
 *
 * Revision 1.2  2003/04/24 14:55:53  lipari
 * *** empty log message ***
 * 
 */
#ifndef __KERNELFACTORY_HPP__
#define __KERNELFACTORY_HPP__

#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <vector>

namespace yaks {

    typedef std::string defaultIDKeyType;

    // The abstract factory itself.
    // Implemented using the Singleton pattern
    template <class manufacturedObj, typename classIDKey=defaultIDKeyType>
    class kernelFactory 
    {
        // a BASE_CREATE_FN is a function that takes no parameters
        // and returns an unique_ptr to a manufactuedObj.  Note that
        // we use no parameters, but you could add them
        // easily enough to allow overloaded ctors, e.g.:
        //   typedef std::unique_ptr<manufacturedObj> (*BASE_CREATE_FN)(int);
        typedef manufacturedObj* (*BASE_CREATE_FN)(const int, const double*, const int);

        // FN_REGISTRY is the registry of all the BASE_CREATE_FN
        // pointers registered.  Functions are registered using the
        // regCreateFn member function (see below).
        typedef std::map<classIDKey, BASE_CREATE_FN> FN_REGISTRY;
        FN_REGISTRY registry;

        // Singleton implementation - private ctor & copying, with
        // no implementation on the copying.
        kernelFactory();
        kernelFactory(const kernelFactory&); // Not implemented
        kernelFactory &operator=(const kernelFactory&); // Not implemented
        public:

        // Singleton access.
        static kernelFactory &instance();

        // Classes derived from manufacturedObj call this function once
        // per program to register the class ID key, and a pointer to
        // the function that creates the class.
        void regCreateFn(const classIDKey &, BASE_CREATE_FN);

        // Create a new class of the type specified by className.
        manufacturedObj* create(const classIDKey&, const int, const double*, const int) const;
    };

    ////////////////////////////////////////////////////////////////////////
    // Implementation details.  If no comments appear, then I presume
    // the implementation is self-explanatory.

    template <class manufacturedObj, typename classIDKey>
    kernelFactory<manufacturedObj, classIDKey>::kernelFactory()
    {
    }

    template <class manufacturedObj, typename classIDKey>
    kernelFactory<manufacturedObj, classIDKey> &kernelFactory<manufacturedObj, classIDKey>::instance()
    {
      // Note that this is not thread-safe!
      static kernelFactory theInstance;
      return theInstance;
    }

    // Register the creation function.  This simply associates the classIDKey
    // with the function used to create the class.  The return value is a dummy
    // value, which is used to allow static initialization of the registry.
    // See example implementations in base.cpp and derived.cpp
    template <class manufacturedObj, typename classIDKey>
    void kernelFactory<manufacturedObj, classIDKey>::regCreateFn(const classIDKey &clName, BASE_CREATE_FN func)
    {
      registry[clName]=func;
    }

    // The create function simple looks up the class ID, and if it's in the list,
    // the statement "(*i).second();" calls the function.
    template <class manufacturedObj, typename classIDKey>
    manufacturedObj* kernelFactory<manufacturedObj, classIDKey>::create(const classIDKey &className, const int num_tasks, const double *ts_table_array, const int time_scale) const
    {      
      typename FN_REGISTRY::const_iterator regEntry=registry.find(className);

      if (regEntry != registry.end()) {
        return (*regEntry).second(num_tasks, ts_table_array, time_scale);
      }
      return NULL;
    }

    // Helper template to make registration painless and simple.
    template <class ancestorType,
              class manufacturedObj,
              typename classIDKey=defaultIDKeyType>
    class registerInFactory {
      public:
      static ancestorType* createInstance(const int num_tasks, const double *ts_table_array, const int time_scale)
      {
        return manufacturedObj::createInstance(num_tasks, ts_table_array, time_scale);
      }
      registerInFactory(const classIDKey &id)
      {
        kernelFactory<ancestorType>::instance().regCreateFn(id, createInstance);
      }
    };

}

#endif
