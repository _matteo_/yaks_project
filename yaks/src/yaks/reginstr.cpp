/***************************************************************************
    begin                : Thu Apr 24 15:54:58 CEST 2003
    copyright            : (C) 2003 by Giuseppe Lipari
    email                : lipari@sssup.it
 ***************************************************************************/
/***************************************************************************
 *                                                                         *
 *   This file is free software; you can redistribute it and/or            *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 ***************************************************************************/

#include "factory.hpp"
#include "strtoken.hpp"
#include "RandExecSegment.h"
#include "FixedExecSegment.h"

namespace yaks {

    const Segment::BASE_KEY_TYPE ExecName("delay");
    const Segment::BASE_KEY_TYPE FixedName("fixed");

    /** 
        This namespace should never be used by the user. Contains
        functions to initialize the abstract factory that builds
        instructions @see Task::insertCode()
    */ 
    namespace __instr_stub
    {
        static registerInFactory<Segment, RandExecSegment, Segment::BASE_KEY_TYPE>
        registerRandEx(ExecName);
                
        static registerInFactory<Segment, FixedExecSegment, Segment::BASE_KEY_TYPE>
        registerFixedEx(FixedName);
    }

    void __reginstr_init() {}

} // namespace yaks

