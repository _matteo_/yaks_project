/***************************************************************************
    begin                : Thu Apr 24 15:54:58 CEST 2003
    copyright            : (C) 2003 by Giuseppe Lipari
    email                : lipari@sssup.it
 ***************************************************************************/
/***************************************************************************
 *                                                                         *
 *   This file is free software; you can redistribute it and/or            *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 ***************************************************************************/

#ifndef __REGINSTR_HPP__
#define __REGINSTR_HPP__

namespace yaks {
    void __reginstr_init();
    void __regsched_init();
    void __regtask_init();
}

#endif
