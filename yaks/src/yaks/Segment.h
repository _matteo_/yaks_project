/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2014, ReTiS Lab., Scuola Superiore Sant'Anna.
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the ReTiS Lab. nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

/**
 * \file Segment.h
 * \brief The base class for every pseudo instruction
 * \author Matteo Morelli matteo.morelli@sssup.it
 * \date December 2013
 */
#ifndef YAKS_SEGMENT_HDR
#define YAKS_SEGMENT_HDR
#include <iostream>
#include <string>

namespace yaks
{
    /**
     * \addtogroup yaks_core_tasks
     * @{
     */
    /**
     * \brief The base class for every pseudo instruction
     */
    class Segment
    {

    public:

        typedef std::string BASE_KEY_TYPE;

        /**
         * \brief Default constructor
         */
        Segment();

        /**
         * \brief The (pure) virtual destructor
         */
        virtual ~Segment();

        /**
         * Get the worst-case execution time for this instruction
         */
        virtual double getWCET() const = 0;

        /**
         * Get the total computation time of the instruction 
         */
        virtual double getDuration() const = 0;

        /**
         * \brief Insertion (<<) operator
         */
        friend std::ostream& operator<<(std::ostream&, const Segment&);

    };
    /** @} */

}

#endif // YAKS_SEGMENT_HDR
