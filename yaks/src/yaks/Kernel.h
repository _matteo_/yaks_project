/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2014, ReTiS Lab., Scuola Superiore Sant'Anna.
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the ReTiS Lab. nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

/**
 * \file Kernel.h
 * \brief Object adapter for RTSim kernels
 * \authors:
 * \        Matteo Morelli matteo.morelli@sssup.it
 * \        Fabio Cremona fabio.cremona@sssup.it
 * \        Marco Di Natale marco.dinatale@sssup.it
 * \date February 2014
 */

#ifndef YAKS_KERNEL_HDR
#define YAKS_KERNEL_HDR
#include <string>
#include <map>
#include <vector>
#include "Event.h"
#include "SimTask.h"

namespace yaks
{
    /**
     * \addtogroup yaks_interface
     * @{
     */
    /**
     * \brief The base class for every kernel
     */
    class Kernel
    {

    public:

        typedef std::string BASE_KEY_TYPE;

        /**
         * \brief The virtual destructor
         */
        virtual ~Kernel();

        //------------------------------------------------------------
        // TODO. The declaration of the S-Function API virtual methods
        //       goes here!
        //------------------------------------------------------------
        /**
         * Initializations for the RT Simulation Engine
         * // TODO. Other names:
         *          - initializeKernel()
         *          - initializeKernelInternals()
         *          - initializeKernelEvents()      <--
         *          - initializeKernelSimulation()
         *          - ...
         */        
        virtual void initializeSimulation(const double, const double* const*) = 0;

        /**
         * Process the next simulation step
         */
        virtual void processNextEvent() = 0;

        /**
         * Return the next event in the RT simulation engine queue to be processed
         */        
        virtual yaks::Event* getNextEvent() = 0;

        /**
         * Return the RT-Simulator time of the next event in the engine event queue
         */
        virtual int getTimeOfNextEvent() = 0;

        /**
         * Return the RT-Simulator time at which Kernel execution will be triggered again
         */
        virtual int getNextWakeUpTime() = 0;

        /**
         * Update the list of running tasks
         */
        virtual void getRunningTasks() = 0;

        /**
         * Activate the set of aperiodic tasks corresponding to the activation request indices
         */
        virtual void activateAperiodicTasks(std::vector<int>&, int) = 0;

        /* ================================================================== */

        // Add new scheduled tasks to the list of tasks to be scheduled
        void addNewTasksToTriggerQueue();
        
        // Mark running tasks
        void markNewScheduledTasks();
        
        // Clear the new Job Task Flag
        void clearStartTaskMark(SimTask *task);
        
        // Add a task to the trigger queue
        void addTaskToTriggerQueue(SimTask *task);

        // Return the Simulink port of Task taskID
        int getPort(SimTask *task);

        // Return the a vector of Simulink ports to trigger
        // And then clear the list
        std::vector<int> getPortsToTrigger();
        
        int getFlag(std::string task) {return _jobs_status[task];};

    protected:

        // map of the task-index/block-port correspondence
        std::map<std::string, int> _task_port_map;

        // map the aperiodic request index into the corresponding task (index)
        std::map<int, int> _aper_req_task_map;

        std::vector<std::string> _tasks_to_trigger;
        
        std::vector<std::string> _running_tasks;

        std::map<std::string, int> _jobs_status; // 0 = new
                                                 // 1 = old

    };
    /** @} */

}

#endif // YAKS_KERNEL_HDR
