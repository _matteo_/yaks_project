/*-----------------------------------------------------------------------------------
 *  Copyright (C) 2014  ReTiS Lab., Scuola Superiore Sant'Anna
 *
 *  This file is part of yaks_rtsim.
 *
 *  yaks_rtsim is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  RTSS is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with RTSS; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *--------------------------------------------------------------------------------- */

/**
 * \file EventRtSim.h
 * \brief Object adapter for RTSim kernels
 * \authors:
 * \        Matteo Morelli matteo.morelli@sssup.it
 * \        Fabio Cremona fabio.cremona@sssup.it
 * \        Marco Di Natale marco.dinatale@sssup.it
 * \date February 2014
 */

#ifndef YAKS_EVENTRTSIM_HDR
#define YAKS_EVENTRTSIM_HDR
#include <string>
#include <yaks/Event.h>  // yaks::Event
#include <event.hpp>     // MetaSim::Event
#include "SimTaskRtSim.h"

namespace yaks
{
    /**
     * \addtogroup yaks_adapter
     * @{
     */
    /**
     * \brief Object adapter for RTSim/MetaSim events
     */
    class EventRtSim : public yaks::Event
    {

    protected:

        MetaSim::Event *_ms_evt; // The base event representation in RTSim/MetaSim (Adaptee)
        SimTaskRtSim _gen_task;  // RTSim task which has generated the event

    public:

        /**
         * \brief Default constructor
         */
        EventRtSim();

        /**
         * \brief Construction from a MetaSim::Event
         */
        EventRtSim(MetaSim::Event*);

        /**
         * \brief The virtual destructor
         */
        virtual ~EventRtSim();

        /**
         * \brief Set the pointer to the RtSim/MetaSim event (Adaptee)
         */
        virtual void setAdapteePtr(MetaSim::Event*);

        /**
         * \brief Get event name
         */
        virtual std::string getName() const;

        /**
         * \brief Get time of event occurrence
         * TODO. return int?
         */
        virtual long int getTime() const;

        /**
         * \brief Get the event type
         */
        virtual EventType getType() const;

        /**
         * \brief Get a pointer to the task which has generated this event
         */
        virtual SimTask* getGeneratorTask();

    };
    /** @} */

}

#endif // YAKS_EVENTRTSIM_HDR
