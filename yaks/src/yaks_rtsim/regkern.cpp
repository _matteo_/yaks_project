/*-----------------------------------------------------------------------------------
 *  Copyright (C) 2014  ReTiS Lab., Scuola Superiore Sant'Anna
 *
 *  This file is part of yaks_rtsim.
 *
 *  yaks_rtsim is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  RTSS is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with RTSS; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *--------------------------------------------------------------------------------- */

#include <yaks/factory.hpp>
#include "KernelRtSim.h"

namespace yaks {

    const Kernel::BASE_KEY_TYPE KernRtSimName("rtsim");

    /** 
        This namespace should never be used by the user. Contains
        functions to initialize the abstract factory that builds
        kernels
    */ 
    namespace __kern_stub
    {
        static registerInFactory<Kernel, KernelRtSim, Kernel::BASE_KEY_TYPE>
        registerKernRtSim(KernRtSimName);
    }

    //void __reginstr_init() {}

} // namespace yaks

