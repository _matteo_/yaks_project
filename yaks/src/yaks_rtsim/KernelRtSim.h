/*-----------------------------------------------------------------------------------
 *  Copyright (C) 2014  ReTiS Lab., Scuola Superiore Sant'Anna
 *
 *  This file is part of yaks_rtsim.
 *
 *  yaks_rtsim is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  RTSS is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with RTSS; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *--------------------------------------------------------------------------------- */

/**
 * \file KernelRtSim.h
 * \brief Object adapter for RTSim kernels
 * \authors:
 * \        Matteo Morelli matteo.morelli@sssup.it
 * \        Fabio Cremona fabio.cremona@sssup.it
 * \        Marco Di Natale marco.dinatale@sssup.it
 * \date February 2014
 */

#ifndef YAKS_KERNELRTSIM_HDR
#define YAKS_KERNELRTSIM_HDR
#include <vector>
#include <string>
#include <task.hpp>      // RTSim::Task
#include <scheduler.hpp> // RTSim::Scheduler
#include <kernel.hpp>    // RTSim::RTKernel
#include <texttrace.hpp> // RTSim::TextTrace
#include <jtrace.hpp>    // RTSim::JavaTrace
#include <yaks/Kernel.h> // yaks::Kernel
#include "EventRtSim.h"

namespace yaks
{
    /**
     * \addtogroup yaks_adapter
     * @{
     */
    /**
     * \brief Object adapter for RTSim kernels
     */
    class KernelRtSim : public yaks::Kernel
    {

    public:

        /**
         * \brief Creator function used  for object construction
         * according to the Factory Method pattern
         *
         *   TODO. XML description of kernel properties:
         *       - scheduler type
         *       - task (set) type
         *       - cpu number/type
         *       - other
         */
        //static yaks::Kernel* create(const TiXmlElement* xml_elem);

        /**
         * \brief Creator function used  for object construction
         * according to the Factory Method pattern
         */
        static yaks::Kernel* createInstance(std::vector<std::string>&);

        /**
         * \brief The virtual destructor
         */
        virtual ~KernelRtSim();

        //------------------------------------------------------------
        // TODO. The declaration of the S-Function API virtual methods
        //       goes here!
        //------------------------------------------------------------
        
        virtual void initializeSimulation(const double, const double * const *);
        
        virtual void processNextEvent();
        
        virtual yaks::Event* getNextEvent();
        
        virtual int getTimeOfNextEvent();
        
        virtual int getNextWakeUpTime();
        
        virtual void getRunningTasks();   

        virtual void activateAperiodicTasks(std::vector<int>&, int);

    protected:

        //RTSim::MRTKernel *_rts_kern;           
        RTSim::RTKernel *_rts_kern;           // The base kernel representation in RTSim (Adaptee)

        // Bunch of RTSim entities used by RTSim::RTKernel
        // that are _NOT_ deleted by RTSim::~RTKernel() --
        // And have to be manually managed (instantiated/deleted)
        // by this Adapter
        RTSim::Scheduler *_rts_sched;         // The real-time scheduler
        RTSim::ResManager *_rts_resMng;       // The resource manager (not used for now by this adapter)
        std::vector<RTSim::Task*> _rts_tasks; // The (RTSim) task objects making the task set
        EventRtSim _next_event;               // Next RT Sim engine event

        // RTSim Tracers
        RTSim::TextTrace *ttrace;             // Enable a textual representation of the simulation trace
        RTSim::JavaTrace *jtrace;             // Enable a graphical representation of the simulation trace
        
    private:

        /**
         * \brief Prevent default construction
         */
        KernelRtSim();

        /**
         * \brief Construct from XML entry
         *
         * Prevent construction via direct call to the constructor
         * (using the Factory Method pattern)
         *
         * TODO. XML description of kernel properties (See above)
         */
        //KernelRtSim(const TiXmlElement* xml_elem);

        /**
         * \brief Construct from external parameters
         */
        KernelRtSim(const std::string&, const int, const std::vector<std::string>&);

    };
    /** @} */

}

#endif // YAKS_KERNELRTSIM_HDR
