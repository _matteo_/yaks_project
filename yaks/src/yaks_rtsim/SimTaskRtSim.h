/*-----------------------------------------------------------------------------------
 *  Copyright (C) 2014  ReTiS Lab., Scuola Superiore Sant'Anna
 *
 *  This file is part of yaks_rtsim.
 *
 *  yaks_rtsim is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  RTSS is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with RTSS; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *--------------------------------------------------------------------------------- */

/**
 * \file SimTaskRtSim.h
 * \brief Object adapter for RTSim kernels
 * \authors:
 * \        Matteo Morelli matteo.morelli@sssup.it
 * \        Fabio Cremona fabio.cremona@sssup.it
 * \        Marco Di Natale marco.dinatale@sssup.it
 * \date February 2014
 */

#ifndef YAKS_SIMTASKRTSIM_HDR
#define YAKS_SIMTASKRTSIM_HDR
#include <string>
#include <yaks/SimTask.h> // yaks::SimTask
#include <task.hpp>       // RTSim::Task

namespace yaks
{
    /**
     * \addtogroup yaks_adapter
     * @{
     */
    /**
     * \brief Object adapter for RTSim/MetaSim tasks
     */
    class SimTaskRtSim : public yaks::SimTask
    {

    protected:

        RTSim::Task *_rts_task; // The base task representation in RTSim/MetaSim (Adaptee)

    public:

        /**
         * \brief Default constructor
         */
        SimTaskRtSim();

        /**
         * \brief Construction from a RTSim::Task
         */
        SimTaskRtSim(RTSim::Task*);

        /**
         * \brief The virtual destructor
         */
        virtual ~SimTaskRtSim();

        /**
         * \brief Set the pointer to the RtSim/MetaSim task (Adaptee)
         */
        virtual void setAdapteePtr(RTSim::Task*);

        /**
         * \brief Get the unique ID of this task
         */
        virtual std::string getUID() const;

        /**
         * Check if task has no instructions
         */ 
        virtual bool isEmpty();

        /**
         * \brief Discard all instructions
         */
        virtual void discardInstructions();

        /**
         * \brief Kernel Add an instruction of fixed length to a given task
         */
        virtual void addInstruction(int duration);

    };
    /** @} */

}

#endif // YAKS_SIMTASKRTSIM_HDR
