% (simple) task set description (now set for use with rtsim)
%
% yaks:  [priority, period           ]
% rtsim: [period,   relative deadline]
%task_set_descr = [25, 20; 0, 30];  % old (no longer supported) yaks
%task_set_descr = [20, 20; 30, 30]; % simple (obsolete, no longer supported) rtsim interface
%
%                 % type          %iat  %rdl   %ph
task_set_descr = {'PeriodicTask',   20,   20,    0; ...
                  'AperiodicTask',   0,    5,    0; ...
                  'PeriodicTask',   30,   30,    0};

% sequences of pseudo instructions (task codes)
t1_instrs = {'fixed(2)', 'fixed(3)'};
t2_instrs = {'fixed(1)'; 'fixed(2)'};
t3_instrs = {'fixed(3)'; 'fixed(7)'; 'fixed(7)'};

% launch the simulink model
uiopen('yaks_dummy_aperiodic.mdl',1);
