% (simple) task set description (now set for use with rtsim)
%
% yaks:  [priority, period           ]
% rtsim: [period,   relative deadline]
%task_set_descr = [24, 0.004; 12, 0.005; 0, 0.006];           % yaks
%task_set_descr = [0.004, 0.004; 0.005, 0.005; 0.006, 0.006]; % (obsolete) rtsim interface
%                 % type           %iat   %rdl   %ph
task_set_descr = {'PeriodicTask', 0.004, 0.004,    0; ...
                  'PeriodicTask', 0.005, 0.005,    0; ...
                  'PeriodicTask', 0.006, 0.006,    0};

% sequences of pseudo instructions (task codes)
t1_descr = {'fixed(0.002)'};
t2_descr = {'fixed(0.002)'};
t3_descr = {'fixed(0.002)'};

% launch the simulink model
uiopen('yaks_threeservos.mdl',1);