% (simple) task set description (now set for use with rtsim)
%
% yaks:  [priority, period           ]
% rtsim: [period,   relative deadline]
%task_set_descr = [25, 20; 0, 30]; % yaks
%task_set_descr = [20, 20; 30, 30]; % simple (obsolete, no longer supported) rtsim interface
%
%                 % type          %iat  %rdl   %ph
task_set_descr = {'PeriodicTask',   20,   20,    0; ...
                  'PeriodicTask',   30,   30,    0};

% sequences of pseudo instructions (task codes)
t1_instrs = {'delay(unif(2,3))', 'delay(unif(3,4))'};
t2_instrs = {'delay(unif(3,4))'; 'delay(unif(7,8))'; 'delay(unif(7,8))'};

% launch the simulink model
uiopen('yaks_dummy_random.mdl',1);
