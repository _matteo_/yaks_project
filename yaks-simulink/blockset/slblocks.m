function blkStruct = slblocks
  % Specify that the product should appear in the library browser
  % and be cached in its repository
  Browser.Library = 'yaks';
  Browser.Name    = 'Yet Another Kernel Simulation (yaks) Package';
  blkStruct.Browser = Browser;