/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2014, ReTiS Lab., Scuola Superiore Sant'Anna.
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the ReTiS Lab. nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

#include <yaks/factory.hpp>
#include <yaks/genericvar.hpp>

namespace yaks {

	const RandomVar::BASE_KEY_TYPE DeltaName("delta");
    const RandomVar::BASE_KEY_TYPE UnifName1 ("unif");
    const RandomVar::BASE_KEY_TYPE UnifName2 ("uniform");
    const RandomVar::BASE_KEY_TYPE NormalName1 ("normal");
    const RandomVar::BASE_KEY_TYPE NormalName2 ("gauss");
    const RandomVar::BASE_KEY_TYPE ExponentialName1 ("exp");
    const RandomVar::BASE_KEY_TYPE ExponentialName2 ("exponential");
    const RandomVar::BASE_KEY_TYPE ParetoName ("pareto");
    const RandomVar::BASE_KEY_TYPE PoissonName ("poisson");
    const RandomVar::BASE_KEY_TYPE DetName ("trace");
    const RandomVar::BASE_KEY_TYPE GenericName ("PDF");

    /**
       This namespace should not be visible, and in any case, users
       should never access objects of this namefile. This is used just
       for initialization of the objects needed for the abstract
       factory that creates RandomVars.
    */
    namespace __var_stub
    {
        static registerInFactory<RandomVar, 
                                 DeltaVar,
                                 RandomVar::BASE_KEY_TYPE>
        registerDelta(DeltaName);
    
        static registerInFactory<RandomVar,
                                 UniformVar,
                                 RandomVar::BASE_KEY_TYPE>
        registerUnif1(UnifName1);
    
        static registerInFactory<RandomVar,
                                 UniformVar,
                                 RandomVar::BASE_KEY_TYPE>
        registerUnif2(UnifName2);
  
        static registerInFactory<RandomVar,
                                 NormalVar,
                                 RandomVar::BASE_KEY_TYPE>
        registerNormal1(NormalName1);
  
        static registerInFactory<RandomVar,
                                 NormalVar,
                                 RandomVar::BASE_KEY_TYPE>
        registerNormal2(NormalName2);
  
        static registerInFactory<RandomVar,
                                 ExponentialVar,
                                 RandomVar::BASE_KEY_TYPE>
        registerExp1(ExponentialName1);
  
        static registerInFactory<RandomVar,
                                 ExponentialVar,
                                 RandomVar::BASE_KEY_TYPE>
        registerExp2(ExponentialName2);
    
        static registerInFactory<RandomVar,
                                 ParetoVar,
                                 RandomVar::BASE_KEY_TYPE>
        registerPareto(ParetoName);
        
        static registerInFactory<RandomVar,
                                 PoissonVar,
                                 RandomVar::BASE_KEY_TYPE>
        registerPoisson(PoissonName);
        
        static registerInFactory<RandomVar,
                                 DetVar,
                                 RandomVar::BASE_KEY_TYPE>
        registerDet(DetName);
        
        static registerInFactory<RandomVar,
                                 GenericVar,
                                 RandomVar::BASE_KEY_TYPE>
        registerGeneric(GenericName);

    } // namespace __var_stub

} // namespace yaks
