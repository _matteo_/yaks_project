/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2014, ReTiS Lab., Scuola Superiore Sant'Anna.
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the ReTiS Lab. nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

/**
 * \authors:
 * \        Matteo Morelli matteo.morelli@sssup.it
 * \        Fabio Cremona fabio.cremona@sssup.it
 * \        Marco Di Natale marco.dinatale@sssup.it
 */

#define S_FUNCTION_NAME yaks_kernel
#define S_FUNCTION_LEVEL 2

#include <iostream>
#include <sstream>
#include <memory>
#include <cstdlib>           // atof
#include <yaks/factory.hpp>
#include <yaks/SimTask.h>
#include <yaks/Kernel.h>
#include <yaks/Event.h>

#include "regkern.cpp"      // registration of yaks::Kernel adapters

#include "simstruc.h"
#include "matrix.h"

#define TS_DESCR_VARNAME    0
#define SCHEDULING_POLICY   1
#define SP_DESCR_VARNAME    2
#define DEADLINE_MISS_RULE  3
#define TIME_RESOLUTION     4
#define NUMBER_OF_CORES     5
#define SIMULATION_ENGINE   6

/**
 * \brief Convert cell-array-based entity descriptions into vector-of-strings-based descriptions
 *
 * A number of kernel entities (e.g., task sets, scheduling policies) are described
 * through MATLAB variables that are cell arrays. Such arrays have one or more rows
 * (depending on what they are used for).
 *
 * The first parameter in each row is guaranteed to be a std::string. The other parameters are
 * guaranteed to be numeric or empty (MATLAB) cells. Error checking is performed at MATLAB level
 * by using mask callbacks
 *
 * This function converts cell-array-based entity descriptions into entity descriptions that are
 * made of a vector of strings. Each string represents a row of the cell array. A string is made
 * of a list of parameters separated by the token ';'. Parameters coincide with the entries in
 * the corresponding row of the cell-array.
 */
static std::vector<std::string> cellArrayDescrToVectorOfStrings(const mxArray *mx_var)
{
    // The vector-of-strings-based description (output)
    std::vector<std::string> out_descr;

    // Get the number of entries (rows of an entity description)
    // and the number of parameters
    int num_entries = mxGetM(mx_var);
    int num_params = mxGetN(mx_var);

    // Convert the description of each task
    for (int i = 0; i < num_entries; ++i)
    {
        // The stringstream
        std::stringstream ss;

        // Put the entity type into the stringstream
        mxArray *entity_type = mxGetCell(mx_var, i);
        char *type_name = new char[mxGetN(entity_type)+1];
        mxGetString(entity_type, type_name, mxGetN(entity_type)+1);
        ss << type_name;
        delete type_name;

        // Put the other parameters (numerical) into the stringstream
        for (int j = 1; j < num_params; ++j)
        {
            mxArray *entity_p = mxGetCell(mx_var, (i + num_entries * j));

            // if the cell is _not_ empty
            if (mxGetM(entity_p)*mxGetN(entity_p) != 0)
            {
                double *param = mxGetPr(entity_p);
                ss << ';' << *param;
            }
        }

        // Put the last separator (otherwise the last parameter is ignored!)
        ss << ';';

        // Insert the stringstream into the description vector
        out_descr.push_back(ss.str());
    }

    // Return the vector-of-string-based description
    return out_descr;
}

/**
 * \brief Build the list of parameters that configure the yaks::Kernel
 *
 * The yaks::Kernel class (or, more precisely, its underlying concrete
 * implementation) needs to be configured with a bunch of information, such
 * as the description of the task set, the scheduling policy, some computing-platform
 * details like number of CPU cores, and time resolution, to name only a few. This
 * information is stored inside the yaks_kernel block as mask parameters.
 *
 * This function reads the mask parameters and returns a vector-of-string-based
 * description of the information needed by the yaks::Kernel class. Specifically,
 * the information returned by this function has the following form:
 *   - the number of tasks in the task-set (#tasks) - std::string (1)      <-- vector.begin()
 *   - the task-set description                     - std::string (#tasks)
 *   - the scheduling policy description            - std::string (1)
 *   - the number of CPU cores                      - std::string (1)
 *   - the time resolution                          - std::string (1)
 *                                                                         <-- vector.end()
 *
 * No err checking is performed when reading mask parameters since these are already
 * guaranteed to be valid at MATLAB level (err checking performed by mask callbacks).
 */
static std::vector<std::string> readMaskAndBuildConfVector(SimStruct *S)
{
    char *bufTsd,       // TS_DESCR_VARNAME
         *bufSchedPol,  // SCHEDULING_POLICY
         *bufSpd,       // SP_DESCR_VARNAME
         *bufDeadMiss,  // DEADLINE_MISS_RULE
         *bufTimeRes;   // TIME_RESOLUTION
    int bufTsdLen, bufSchedPolLen, bufSpdLen, bufDeadMissLen, bufTimeResLen;
    std::vector<std::string> kern_params; // The return list of parameters
    std::stringstream ss;                 // A convenience stringstream

    // Get the number of tasks (it is equal to the size of output port)
    int_T num_tasks = ssGetOutputPortWidth(S,0);

    // Get the name of the workspace variable for the task set
    bufTsdLen = mxGetN( ssGetSFcnParam(S,TS_DESCR_VARNAME) )+1;
    bufTsd = new char[bufTsdLen];
    mxGetString(ssGetSFcnParam(S,TS_DESCR_VARNAME), bufTsd, bufTsdLen);

    // Get the actual task set description
    std::vector<std::string> ts_descr = cellArrayDescrToVectorOfStrings(mexGetVariablePtr("base", bufTsd));
    delete bufTsd;

    // **Insert** the number of tasks and
    //            the task set description in the return list
    ss << num_tasks;
    kern_params.push_back(ss.str());
    ss.str(std::string());                    // Flush the ss
    kern_params.insert(kern_params.begin()+1,
                           ts_descr.begin(),
                               ts_descr.end());

    // Get the scheduling policy
    bufSchedPolLen = mxGetN( ssGetSFcnParam(S,SCHEDULING_POLICY) )+1;
    bufSchedPol = new char[bufSchedPolLen];
    mxGetString(ssGetSFcnParam(S,SCHEDULING_POLICY), bufSchedPol, bufSchedPolLen);
    std::vector<std::string> sp_descr;
	sp_descr.push_back(std::string(bufSchedPol) + ';');
    delete bufSchedPol;

    // Check if it's a custom sched. policy
    if (sp_descr[0] == "OTHER;")
    {
        // Get the name of the workspace variable for the custom sched. policy
        bufSpdLen = mxGetN( ssGetSFcnParam(S,SP_DESCR_VARNAME) )+1;
        bufSpd = new char[bufSpdLen];
        mxGetString(ssGetSFcnParam(S,SP_DESCR_VARNAME), bufSpd, bufSpdLen);

        // Get the actual custom sched. policy description
        // (It's a std::vector<std::string> with size() == 1)
        sp_descr = cellArrayDescrToVectorOfStrings(mexGetVariablePtr("base", bufSpd));
        delete bufSpd;
    }

    // **Insert** the sched. policy description in the return list
    kern_params.push_back(sp_descr[0]);

    // TODO. Get the scheduler action on deadline miss
    // TODO. **Insert** the scheduler action on deadline miss in the return list

    // Get the number of CPU cores parameter
    int num_cores = static_cast<int>(mxGetScalar(ssGetSFcnParam(S, NUMBER_OF_CORES)));

    // **Insert** the number of cores in the return list
    ss << num_cores;
    kern_params.push_back(ss.str());
    ss.str(std::string());                    // Flush the ss

    // Get the time resolution
    bufTimeResLen = mxGetN( ssGetSFcnParam(S,TIME_RESOLUTION) )+1;
    bufTimeRes = new char[bufTimeResLen];
    mxGetString(ssGetSFcnParam(S,TIME_RESOLUTION), bufTimeRes, bufTimeResLen);

    // Convert the time resolution to a double
    std::string time_resolution(bufTimeRes);
    delete bufTimeRes;
    if (time_resolution == "Seconds")
        ss << 1.0;
    else if (time_resolution == "Milli_Seconds")
        ss << 1.0e3;
    else if (time_resolution == "Micro_Seconds")
        ss << 1.0e6;
    else if (time_resolution == "Nano_Seconds")
        ss << 1.0e9;

    // **Insert** the time resolution in the return list
    kern_params.push_back(ss.str());
    ss.str(std::string());                    // Flush the ss

    // Done, return to the caller
    return (kern_params);
}

namespace _yaks_kernel {

    /**
     * \brief Aperiodic-activation requests' Manager
     * \note Internal use
     */
    struct _AperiodicReqsManager
    {
        // data
        bool receivedAperiodicReq;
        std::vector<boolean_T> prev_reqs;
        std::vector<int> aper_activ_idx;
        // methdos
        _AperiodicReqsManager(int, InputBooleanPtrsType);
        ~_AperiodicReqsManager();
        void evaluateIncomingReqs(InputBooleanPtrsType);
    };
    _AperiodicReqsManager::_AperiodicReqsManager(int num_aper_reqs, InputBooleanPtrsType aper_reqs)
    {
		receivedAperiodicReq = false;
        prev_reqs.assign(*aper_reqs, *aper_reqs+num_aper_reqs);
        aper_activ_idx.reserve(num_aper_reqs);
    }
    _AperiodicReqsManager::~_AperiodicReqsManager()
    {
    }
    void _AperiodicReqsManager::evaluateIncomingReqs(InputBooleanPtrsType aper_reqs)
    {
        int num_aper_reqs = prev_reqs.size();
        aper_activ_idx.clear();
        for (int i = 0; i < num_aper_reqs; ++i)
        {
            if (*aper_reqs[i] > prev_reqs[i])
                aper_activ_idx.push_back(i);
        }
        receivedAperiodicReq = (aper_activ_idx.size() > 0);
        prev_reqs.assign(*aper_reqs, *aper_reqs+num_aper_reqs);
    }

}

/* Function: mdlInitializeSizes ===========================================
 * Abstract:
 *    The sizes information is used by Simulink to determine the S-function
 *    block's characteristics (number of inputs, outputs, states, etc.).
 */
#define MDL_INIT_SIZE
static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, 7);  /* Number of expected parameters */

#ifndef YAKS_DISABLE_MASK_PROTECTION
    // Perform mask params validity check
    // TODO.
    //  - more MATLAB vars (custom sched pol, other?)
    //  - the hidden valid_mask parameter
    const mxArray *mxTsdVarName = ssGetSFcnParam(S,TS_DESCR_VARNAME);
    if ((mxGetM(mxTsdVarName) != 1) || (mxGetN(mxTsdVarName) == 0))
    {
        ssSetErrorStatus(S, "Task-set description variable is not correct");
        return;
    }
#endif

    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    // Set the input port to have a dynamic dimension
    if (!ssSetNumInputPorts(S, 2)) return;

    ssSetInputPortWidth(S, 0, DYNAMICALLY_SIZED);
    if(!ssSetInputPortDataType(S, 0, DYNAMICALLY_TYPED)) return;

    ssSetInputPortWidth(S, 1, DYNAMICALLY_SIZED);
	if(!ssSetInputPortDataType(S, 1, SS_BOOLEAN)) return;

    // Set the input ports as Direct Feed Through
    ssSetInputPortDirectFeedThrough(S, 0, 1);   // TODO. Should we use a
                                                // buffer instead, so that
                                                // DFT is no longer needed?
    ssSetInputPortDirectFeedThrough(S, 1, 1);

    // Set the output port to have a dynamic dimension
    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, DYNAMICALLY_SIZED);

    ssSetNumSampleTimes(S, 1);

    ssSetNumPWork(S, 2);  // store the yaks::Kernel and the _yaks_kernel::_AperiodicReqsManager
    ssSetNumRWork(S, 1);  // store the time_resolution
    ssSetNumNonsampledZCs(S, 1);    // next hit

    ssSetSimStateCompliance(S, USE_DEFAULT_SIM_STATE);

    if (ssGetSimMode(S) == SS_SIMMODE_RTWGEN && !ssIsVariableStepSolver(S))
    {
        ssSetErrorStatus(S, "S-function yaks_kernel.cpp cannot be used with Simulink "
                                "Coder and Fixed-Step Solvers because it contains "
                                    "variable sample time");
        return;
    }
}

#if defined(MATLAB_MEX_FILE)
#define MDL_SET_INPUT_PORT_WIDTH
void mdlSetInputPortWidth(SimStruct *S, int_T port, int_T width)
{
	ssSetInputPortWidth(S, port, width);
	if (port == 0) ssSetOutputPortWidth(S, 0, width);
}

#define MDL_SET_OUTPUT_PORT_WIDTH
void mdlSetOutputPortWidth(SimStruct *S, int_T port, int_T width)
{
}
#endif

/* Function: mdlInitializeSampleTimes =====================================
 */
#define MDL_INIT_ST
static void mdlInitializeSampleTimes(SimStruct *S)
{
    // Sample time
    ssSetSampleTime(S, 0, CONTINUOUS_SAMPLE_TIME);
    ssSetOffsetTime(S, 0, FIXED_IN_MINOR_STEP_OFFSET);

    // Get the number of tasks (it is equal to the size of output port)
    int_T num_tasks = ssGetOutputPortWidth(S,0);

    // Initialize the Function Call output
    for (int i = 0; i < num_tasks; i++)
        ssSetCallSystemOutput(S, i);
}

// Function: mdlStart =====================================================
// Abstract:
//   This function is called once at start of model execution. If you
//   have states that should be initialized once, this is the place
//   to do it.
#define MDL_START
static void mdlStart(SimStruct *S)
{
    char *bufSimEng;  // SIMULATION_ENGINE
    int bufSimEngLen;

    // Build the list of parameters that configure the yaks::Kernel
    std::vector<std::string> kern_params = readMaskAndBuildConfVector(S);

    // Get the type of the adapter, i.e., the concrete implementation of yaks::Kernel
    bufSimEngLen = mxGetN( ssGetSFcnParam(S,SIMULATION_ENGINE) )+1;
    bufSimEng = new char[bufSimEngLen];
    mxGetString(ssGetSFcnParam(S,SIMULATION_ENGINE), bufSimEng, bufSimEngLen);
    std::string engine(bufSimEng);
    delete bufSimEng;

    // Instantiate the concrete representation of yaks::Kernel
    std::unique_ptr<yaks::Kernel> kern = yaks::genericFactory<yaks::Kernel>::instance()
                                            .create(engine, kern_params);

    // Save the C++ object to the pointers vector
    yaks::Kernel *_kern = kern.release();
    ssGetPWork(S)[0] = _kern;

    // Save the (C++) manager of aperiodic requests
	ssGetPWork(S)[1] = new _yaks_kernel::_AperiodicReqsManager(ssGetInputPortWidth(S,1),
                                                                (InputBooleanPtrsType) ssGetInputPortSignalPtrs(S,1));

    // Get the time resolution (actually, its floating point representation)
    std::vector<std::string>::iterator it = kern_params.end()-1;
    double time_resolution = atof((*it).c_str());

    // Save the time resolution to the real vector workspace
    ssGetRWork(S)[0] = time_resolution;
}

/* Function: mdlInitializeConditions ======================================
 * Abstract:
 *    Initialize discrete state to zero.
 */
#define MDL_INITIALIZE_CONDITIONS
static void mdlInitializeConditions(SimStruct *S)
{
    // Get the C++ object back from the pointers vector
    yaks::Kernel *kern = static_cast<yaks::Kernel *>(ssGetPWork(S)[0]);

    // Get the time resolution back from the real vector workspace
    double time_resolution = ssGetRWork(S)[0];

    // Get a pointer to the input port
    InputRealPtrsType u = ssGetInputPortRealSignalPtrs(S,0);

    // Initializes the kernel for the simulation
    kern->initializeSimulation(time_resolution, u);
}

/* Function: mdlOutputs ===================================================
 */
#define MDL_OUTPUT
static void mdlOutputs(SimStruct *S, int_T tid)
{
    // Get ports access
    InputRealPtrsType  u = ssGetInputPortRealSignalPtrs(S,0);
    InputBooleanPtrsType aper_reqs = (InputBooleanPtrsType) ssGetInputPortSignalPtrs(S,1);

    // Get the C++ object back from the pointers vector
    yaks::Kernel *kern = static_cast<yaks::Kernel *>(ssGetPWork(S)[0]);
    _yaks_kernel::_AperiodicReqsManager *aper_reqs_mgr = static_cast<_yaks_kernel::_AperiodicReqsManager *>(ssGetPWork(S)[1]);

    // Get the time resolution back from the real vector workspace
    double time_resolution = ssGetRWork(S)[0];

#ifdef YAKS_DEBUG_1
mexPrintf("\n%s: at _time_: %.3f\n", __FUNCTION__, ssGetT(S));
#endif

    // Manage aperiodic activation requests (if any)
    // Note: Aperiodic requests generate new events in the kernel
    // event queue, therefore they must processed _before_
    // computing the next_hit_tick
    aper_reqs_mgr->evaluateIncomingReqs(aper_reqs);
    if (aper_reqs_mgr->receivedAperiodicReq)
    {
#ifdef YAKS_DEBUG_1
mexPrintf("\n*** Aperiodic activ(s) at time: %.3f***\n", ssGetT(S));
#endif
        kern->activateAperiodicTasks(aper_reqs_mgr->aper_activ_idx, ssGetT(S)*(time_resolution));
    }

    // Save the time of next block hit
    long int next_hit_tick = kern->getNextWakeUpTime();

    // If the current time is greater or equal than the next block hit
    if (ssGetT(S) - next_hit_tick/(time_resolution) >= 0.0)
    {
        do
        {
            yaks::Event *e = kern->getNextEvent();
            yaks::SimTask *t = e->getGeneratorTask();

            // Classify the first incoming kernel event
            // within the time-span
            switch (e->getType())
            {
                case yaks::EventType::END_INSTRUCTION:
                {
                    // Add this task to the to-be-triggered task queue
                    // (due to an end instruction)
                    kern->addTaskToTriggerQueue(t);

                    // Read the duration of the next time-consuming activity
                    // for the SimTask
                    real_T duration = *u[kern->getPort(t)];
                    if (duration > 0.0)
                    {
                        // The task is _not_ completed, so add another instruction
                        t->addInstruction(duration*(time_resolution));
                    }
                    break;
                }

                case yaks::EventType::END_TASK:
                {
                    // On Task completion, clear the Start flag of the task
                    kern->clearStartTaskMark(t);

                    // Clear the instruction queue of the task
                    t->discardInstructions();

                    // Initialize the task with the duration of first instruction
                    // (Note that the following relationship holds for tasks
                    // that have completed their execution: duration < 0.0)
                    // Read the duration of the next time-consuming activity
                    // for the SimTask
                    real_T duration = *u[kern->getPort(t)];
                    t->addInstruction(-duration*(time_resolution));
                    break;
                }

                default:
                    break;
            }

            // Process the next event in the RT engine queue
            kern->processNextEvent();

        }
        while ( kern->getTimeOfNextEvent() == next_hit_tick );

        // Update the list of running tasks
        kern->getRunningTasks();

        // Add new scheduled tasks to the list of tasks to be scheduled
        kern->addNewTasksToTriggerQueue();

        // Mark running tasks
        kern->markNewScheduledTasks();

        // Read which tasks have to be triggered
        std::vector<int> ports = kern->getPortsToTrigger();

        // For each Task to trigger, send a Function generation
        // signal onto the corresponding port
        for (std::vector<int>::iterator port = ports.begin();
                port != ports.end();
                    ++port)
        {
            ssCallSystemWithTid(S, *port, tid);
        }
    }
}

/* Function: mdlUpdate ====================================================
 */
#define MDL_UPDATE
static void mdlUpdate(SimStruct *S, int_T tid)
{
    //std::cout << __FILE__ << " - " << __FUNCTION__ << " @" << ssGetT(S) << std::endl;

    // Get the C++ object back from the pointers vector
    //yaks::Kernel *kern = static_cast<yaks::Kernel *>(ssGetPWork(S)[0]);

    // TODO. Bufferize duration readings    
}

#define MDL_ZERO_CROSSINGS
static void mdlZeroCrossings(SimStruct *S)
{
    // Get the C++ object back from the pointers vector
    yaks::Kernel *kern = static_cast<yaks::Kernel *>(ssGetPWork(S)[0]);

    // Get the time resolution back from the real vector workspace
    double time_resolution = ssGetRWork(S)[0];

#ifdef YAKS_DEBUG_1
mexPrintf("\n%s at _time_: %.3f\n", __FUNCTION__, ssGetT(S));
#endif

    ssGetNonsampledZCs(S)[0] = kern->getTimeOfNextEvent()/(time_resolution) - ssGetT(S);
}

// Function: mdlTerminate =================================================
// Abstract:
//   In this function, you should perform any actions that are necessary
//   at the termination of a simulation.  For example, if memory was
//   allocated in mdlStart, this is the place to free it.
static void mdlTerminate(SimStruct *S)
{
    mexPrintf("%s - %s @ %.3f\n", __FILE__, __FUNCTION__, ssGetT(S));

    // Get the C++ object back from the pointers vector
    yaks::Kernel *kern = static_cast<yaks::Kernel *>(ssGetPWork(S)[0]);
    _yaks_kernel::_AperiodicReqsManager *aper_reqs_mgr = static_cast<_yaks_kernel::_AperiodicReqsManager *>(ssGetPWork(S)[1]);

    // Call its destructor
    delete kern;
    delete aper_reqs_mgr;
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
