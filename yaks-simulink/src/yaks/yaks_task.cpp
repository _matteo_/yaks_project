/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2014, ReTiS Lab., Scuola Superiore Sant'Anna.
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the ReTiS Lab. nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

/**
 * \authors:
 * \        Matteo Morelli matteo.morelli@sssup.it
 * \        Fabio Cremona fabio.cremona@sssup.it
 * \        Marco Di Natale marco.dinatale@sssup.it
 */

// TODO: license, doxygen
#define S_FUNCTION_NAME yaks_task
#define S_FUNCTION_LEVEL 2

#include <iostream>
#include <string>
#include <vector>
#include <yaks/Task.h>
#include "simstruc.h"

#include "regvar.cpp"
#include "reginstr.cpp"

#define CONF_NAME 0
#define CONF_NAME_PARAM(S) ssGetSFcnParam(S,CONF_NAME)

static const mxArray * get_sfun_param(SimStruct *S, const int par_id)
{
    const mxArray *mx_ret;
    switch(par_id)
    {
        case 0:
        {
            // Get the File/Variable name
            const mxArray *mx_conf_name = CONF_NAME_PARAM(S);
            if ((mxGetM(mx_conf_name) != 1) || (mxGetN(mx_conf_name) == 0))
            {
                ssSetErrorStatus(S, "MATLAB Variable unspecified");
                return NULL;
            }
            char *var_name = new char[mxGetN(mx_conf_name)+1];
            mxGetString(mx_conf_name,
                            var_name,
                                mxGetN(mx_conf_name)+1);
            if (!var_name)
            {
                ssSetErrorStatus(S, "Unexpected error: variable name was set to NULL (?)");
                return NULL;
            }
            mx_ret = mexGetVariablePtr("base", var_name);
            delete var_name;
            if (!mx_ret)
            {
                ssSetErrorStatus(S, "The MATLAB Variable does not exist");
                return NULL;
            }
            if (mxGetClassID(mx_ret) != mxCELL_CLASS)
            {
                ssSetErrorStatus(S, "The MATLAB Variable must be a cell array containing the sequence of pseudo instructions for the task");
                return NULL;
            }
            mwSize total_num_of_cells = mxGetNumberOfElements(mx_ret);
            if ( (total_num_of_cells == 0) ||
                    ((mxGetM(mx_ret) != 1) && (mxGetN(mx_ret) != 1)) )
            {
                ssSetErrorStatus(S, "The task-code description must have at least one element and it must not be a matrix");
                return NULL;
            }
            mwIndex index;
            for (index = 0; index < total_num_of_cells; index++)
            {
                const mxArray *cell_element_ptr = mxGetCell(mx_ret, index);
                // Empty cell
                if (!cell_element_ptr)
                {
                    ssSetErrorStatus(S, "Empty cells are not allowed in the task-code array");
                    return NULL;
                }
                // Not a string
                if (mxGetClassID(cell_element_ptr) != mxCHAR_CLASS)
                {
                    ssSetErrorStatus(S, "Pseudo instructions must be described by strings");
                    return NULL;
                }
            }
            break;
        }

        default:
            break;
    }
    return mx_ret;
}

/* Function: mdlInitializeSizes ===========================================
 * Abstract:
 *    The sizes information is used by Simulink to determine the S-function
 *    block's characteristics (number of inputs, outputs, states, etc.).
 */
static void mdlInitializeSizes(SimStruct *S)
{
    const mxArray * mx_var;
    int num_segments;

    ssSetNumSFcnParams(S, 1);  /* Number of expected parameters */

    // Get the File/Variable name
    if (!(mx_var = get_sfun_param(S, CONF_NAME))) return;

    // Get the number of segments
    num_segments = mxGetNumberOfElements(mx_var);

    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 0)) return;

    if (!ssSetNumOutputPorts(S, 2)) return;
    ssSetOutputPortWidth(S, 0, 2*num_segments);
    ssSetOutputPortWidth(S, 1, 1);

    ssSetNumSampleTimes(S, 1);

    ssSetNumPWork(S, 1); // yaks::Task (set in mdlInitializeConditions())
    ssSetSimStateCompliance(S, USE_DEFAULT_SIM_STATE);

}

/* Function: mdlInitializeSampleTimes =====================================
 */
static void mdlInitializeSampleTimes(SimStruct *S)
{
    const mxArray *mx_var;
    int i, num_segments;

    // Sample times
    ssSetSampleTime(S, 0, INHERITED_SAMPLE_TIME);
    ssSetOffsetTime(S, 0, FIXED_IN_MINOR_STEP_OFFSET);

    // Get the File/Variable name
    if (!(mx_var = get_sfun_param(S, CONF_NAME))) return;
    num_segments = mxGetNumberOfElements(mx_var);

    // Initialize the Function Call output
    for (i = 0; i < 2*num_segments; i++)
        ssSetCallSystemOutput(S,i);
}

#define MDL_INITIALIZE_CONDITIONS
/* Function: mdlInitializeConditions ======================================
 * Abstract:
 *    Initialize discrete state to zero.
 */
static void mdlInitializeConditions(SimStruct *S)
{
    const mxArray *mx_var;
    const mxArray *cell_element_ptr;
    char *char_data_ptr;
    int num_segments;
    std::vector<std::string> instr_list;

    // Get Variable/File name
    // Get the File/Variable name
    if (!(mx_var = get_sfun_param(S, CONF_NAME))) return;

    // Get the number of segments
    num_segments = mxGetNumberOfElements(mx_var);

    // Construct the sequence of pseudo instructions
    // Note that the consistency of the task-code description (cell array)
    // has already been checked in get_sfun_param()
    for (int index = 0; index < num_segments; index++)
    {
        cell_element_ptr = mxGetCell(mx_var, index);
        char_data_ptr = mxArrayToString(cell_element_ptr);
        instr_list.push_back( std::string(char_data_ptr) );
    }

    // Create a new yaks::Task instance from the task-code description vector
    yaks::Task *task = new yaks::Task(instr_list);

    // Save the new C++ object for later usage (mdlOutputs())
    ssGetPWork(S)[0] = task;

    // Initial (real) Output
    real_T  *y = ssGetOutputPortRealSignal(S,1);
#ifdef YAKS_DEBUG_1
        mexPrintf("%s: task->ni_duration is %.3f\n", __FILE__, task->getSegmentDuration());
#endif
    y[0] = task->getSegmentDuration();
}

/* Function: mdlOutputs ===================================================
 */
static void mdlOutputs(SimStruct *S, int_T tid)
{
    // Get the C++ object back from the pointers vector
    yaks::Task *task = static_cast<yaks::Task *>(ssGetPWork(S)[0]);

    // Process the current segment
    int subsys_idx = task->processSegment();

    // Write (duration of next_instr)
    real_T  *y = ssGetOutputPortRealSignal(S,1);
    y[0] = task->getSegmentDuration();

    // Function-call generation (subsys_act_idx)
    if (subsys_idx == 0)
        ssCallSystemWithTid(S, 0, tid);
    else
    {
        ssCallSystemWithTid(S, 2*subsys_idx - 1, tid);
        if (subsys_idx < task->getNumberOfSegments())
            ssCallSystemWithTid(S, 2*subsys_idx, tid);
    }
#ifdef YAKS_DEBUG_1
        mexPrintf("%s: activated segment #%d task->ni_duration is %.3f\n", __FILE__,
			subsys_idx, task->getSegmentDuration());

#endif
}

/* Function: mdlTerminate =================================================
 *    No termination needed, but we are required to have this routine.
 */
static void mdlTerminate(SimStruct *S)
{
    // Retrieve and destroy C++ object
    yaks::Task *task = static_cast<yaks::Task *>(ssGetPWork(S)[0]);
    delete task;
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif

