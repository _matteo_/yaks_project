%-% Prereqs.
%
% export YAKS_INC=/home/matteo/devel/c++/yaks/src
% export YAKS_LIB=/home/matteo/devel/c++/yaks/bld/src
% METAS_LIB=/usr/local/lib
% RTLIB_LIB=/usr/local/lib
% export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$YAKS_LIB/yaks:$YAKS_LIB/yaks_rtsim:$METAS_LIB:$RTLIB_LIB
%-%
YAKS_ADAPTER = 'YAKS_RTSIM';    % User-customizable

% DO NOT MODIFY BELOW!
% ====================
% Env vars.
YAKS_INC   = getenv('YAKS_INC'); % yaks-root/src
YAKS_LIB   = getenv('YAKS_LIB'); % yaks-root/bld/src
if isempty(YAKS_INC),
    error('YAKS_INC must be defined as environment variable');
elseif isempty(YAKS_LIB),
    error('YAKS_LIB must be defined as environment variable');
end

METAS_LIB  = '/usr/local/lib';  % FIXME. HardCoded!
RTLIB_LIB  = '/usr/local/lib';  % FIXME. HardCoded!
YAKS_DBG   = '-g -DYAKS_DEBUG_1 -DYAKS_DISABLE_MASK_PROTECTION';
MEX_CFLAGS = sprintf('CXXFLAGS=''$CXXFLAGS -Wall -O0 --std=c++0x -D%s''', YAKS_ADAPTER);
MEX_INC    = sprintf('-I%s -I%s -I%s', YAKS_INC, ...                 % FIXME. Remove -I%s, -I%s
                                      '/usr/local/include/rtlib',... % FIXME. Remove rtlib
                                      '/usr/local/include/metasim'); % FIXME. Remove metasim
MEX_LIB    = sprintf('-L%s/yaks -lyaks -L%s/yaks_rtsim -lyaks_rtsim -L%s -lmetasim -L%s -lrtlib', YAKS_LIB, YAKS_LIB, METAS_LIB, RTLIB_LIB);
MEX_OUT    = '-outdir libs';
MEX_IN_CLL = {MEX_OUT, MEX_CFLAGS, YAKS_DBG, MEX_INC, MEX_LIB};
MEX_IN_CMD = [sprintf('%s ',MEX_IN_CLL{1:end-1}), MEX_IN_CLL{end}];
MDL_SRC    = {'src/yaks/yaks_task.cpp', ...
              'src/yaks/yaks_kernel.cpp'};
mkdir('libs');
cellfun(@(sfun) eval(sprintf('mex %s %s', MEX_IN_CMD, sfun)), MDL_SRC, 'UniformOutput', true);
clear MEX_CFLAGS LD_PATH YAKS_DBG METAS_LIB RTLIB_LIB YAKS_INC YAKS_LIB YAKS_ADAPTER MEX_INC MEX_LIB MDL_SRC MEX_IN_CLL MEX_IN_CMD MEX_OUT
